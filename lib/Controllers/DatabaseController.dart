import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/Models/CustomerInfo.dart';
class DatabseController {
  FirebaseFirestore _db = FirebaseFirestore.instance;

  var customerName ="";
  var customerSurname = "";
  var customerCity ="";
  static var _documentID = "module-5";

  Future<void> addCustomer(String name, String surname, String city) async {
    final customer = <String, String>{
      CustomerInfo.NAME: name,
      CustomerInfo.SURNAME: surname,
      CustomerInfo.city: city
    };
    _db.collection("shop")
        .doc(_documentID)
        .set(customer, SetOptions(merge: false))
        .onError((error, stackTrace) => print("Error : ${error}"))
        .whenComplete(() => print("Completed Successfully"))
    ;
  }
  Future<void> readCustomer() async {
    await _db.collection("shop").get().then((event) {
      for (var doc in event.docs) {
        if(doc.id == _documentID) {
         customerName = doc.get(CustomerInfo.NAME);
         customerSurname = doc.get(CustomerInfo.SURNAME);
         customerCity = doc.get(CustomerInfo.city);
        }
      }
    });
  }
  Future<void> updateCustomer(String name, String surname, String city) async {
    final customer = <String, String>{
      CustomerInfo.NAME: name,
      CustomerInfo.SURNAME: surname,
      CustomerInfo.city: city
    };
    _db.collection("shop")
        .doc(_documentID)
        .set(customer, SetOptions(merge: true));
  }
  Future<void> deleteCustomer()async {
    _db.collection("shop").doc(_documentID).delete();
  }
}
