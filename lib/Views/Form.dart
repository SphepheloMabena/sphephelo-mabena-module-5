import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase/Controllers/DatabaseController.dart';
import 'package:firebase_core/firebase_core.dart';

class CustomerForm extends StatefulWidget {
  @override
  State<CustomerForm> createState() => _CustomerFormState();
}

class _CustomerFormState extends State<CustomerForm> {
  DatabseController database = new DatabseController();
  var customerName = "";

  var customerSurname = "" ;

  var customerCity = "";

  var nameTextController = TextEditingController();

  var surnameTextController = TextEditingController();

  var cityTextController = TextEditingController();

  var btnStatus ="Add";

  void readCustomers() {
    database.readCustomer().whenComplete(() {
      nameTextController.text = database.customerName;
      cityTextController.text = database.customerCity;
      surnameTextController.text = database.customerSurname;
    });
  }

  @override
  void initState() {
    readCustomers();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text("Customer"),
          backgroundColor: Colors.blueAccent,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top:120),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(

                      controller: nameTextController,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Name",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(
                      controller: surnameTextController,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Surname",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(
                      controller: cityTextController,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                      ),
                      decoration: InputDecoration(
                          labelText: "City",
                          icon: Icon(Icons.location_on),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),

              //add a button here
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: TextButton(
                  onPressed: (){
                    customerName = nameTextController.value.text;
                    customerSurname = surnameTextController.value.text;
                    customerCity = cityTextController.value.text;
                    database.addCustomer(customerName,customerSurname,customerCity);
                    setState(() {

                    });
                  },
                  child: Container(
                    color: Colors.blueAccent,
                    width: 200,
                    height: 40,
                    child: Center(child: Text("Add Customer",
                      style: TextStyle(
                          color: Colors.white
                      ),)),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: TextButton(
                  onPressed: (){


                    customerName = nameTextController.value.text;
                     customerSurname = surnameTextController.value.text;
                     customerCity = cityTextController.value.text;
                    database.updateCustomer(customerName,customerSurname,customerCity);
                    setState(() {

                    });
                  },
                  child: Container(
                    color: Colors.blueAccent,
                    width: 200,
                    height: 40,
                    child: Center(child: Text("Update Customer",
                      style: TextStyle(
                          color: Colors.white
                      ),)),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
             child: TextButton(
                     onPressed: (){
                     nameTextController.text="";
                     surnameTextController.text = "";
                     cityTextController.text = "";
                    database.deleteCustomer();
                    setState(() {

                    });
                  },
                  child: Container(
                    color: Colors.blueAccent,
                    width: 200,
                    height: 40,
                    child: Center(child: Text("Delete Customer",
                      style: TextStyle(
                          color: Colors.white
                      ),)),
                  ),
                ),
              )
            ],
          ),
        )
    );
  }

  @override
  void dispose() {
    nameTextController.dispose();
    surnameTextController.dispose();
    cityTextController.dispose();
  }
}
